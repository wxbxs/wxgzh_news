# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html
from datetime import datetime

from wxgzh_news.api.dicts.wxnews_dict import WxArticlePayloadKeys
from wxgzh_news.baseitems.baseitem import BaseItem
from wxgzh_news.settings import BASE_WEBSITE


class WxgzhNewsItem(BaseItem):
    wxptinfo: str
    wxpt_id: int  # catid

    moduleid: int  # 21
    file: str  # index
    action: str  # add
    itemid: int  # 0
    forward: str  #
    catid: int  # 47044
    title: str
    level: int  # 1
    style: str
    thumb: str
    # linkurl: str
    content: str
    save_remotepic: int  # 1
    clear_link: int  # 1
    introduce_length: int  # 120
    thumb_no: str
    subtitle: str
    introduce: str
    author: str
    copyfrom: str
    fromurl: str
    tag: str
    voteid: str
    status: int  # 3
    note: str
    addtime: str  # 2021-12-16 17:57:12
    areaid: int  # 0
    hits: int  # 0
    fee: str
    template: str
    submit: str  # 确 定

    def __init__(self, wxpt_id, moduleid=63, **kwargs):
        BaseItem.__init__(self, **kwargs)

        self.wxpt_id = wxpt_id
        self.wxptinfo = "1"
        self.catid = 1
        self.moduleid = moduleid
        self.file = "index"
        self.action = "add"
        self.itemid = 0

        self.submit = "确 定"
        self.status = 3
        self.save_remotepic = 1
        self.clear_link = 0
        self.introduce_length = 120
        self.level = 0

        bw = BASE_WEBSITE.rstrip('/')
        self.forward = f"{bw}/admin.php?moduleid={self.moduleid}&wxptinfo={self.wxptinfo}"

        self.addtime = str(datetime.now())[:19]

    @property
    def values(self):
        """
        转换 baseitems 与 payload 之间的 key
        :return:
        """
        values = dict()
        for k, v in self.__dict__.items():
            values[WxArticlePayloadKeys.get(k, k)] = v
        return values
