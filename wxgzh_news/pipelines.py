# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import logging

from itemadapter import ItemAdapter

from wxgzh_news.api.addnews import AddNews


class WxgzhNewsPipeline:

    def __init__(self):
        self.newsAdder = AddNews()

    def process_item(self, item, spider):
        try:
            self.newsAdder.add(payload=item)
        except Exception as e:
            logging.warning(f"Save item failed for item: {item}; reason is {e.args}")
        finally:
            return item