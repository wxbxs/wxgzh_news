# -*- coding: utf-8 -*-
# CreateDate : 2021/12/21 16:15
# Author     : 不肖生
# Github     : https://gitee.com/wxbxs/WxgzhNews
# EditDate   : 
# sourceProj : WxgzhNews
# Description:


gzh_catid_dict = {
    '选择微信平台': '0',
    '中物联采购委': '1',
    '中国物流与采购联合会': '2',
    '中物联现代供应链研究院': '3',
    '林度空间': '6',
    '五道口供应链研究院': '8',
    '供应链架构师': '9',
    '供应链管理专栏': '10',
    '供应链管理云平台': '11',
    '供应链金融': '12',
    '招标采购与供应链管理智库': '13',
    '采购从业者': '14',
}


payload = {
    "moduleid": "63",
    "file": "index",
    "action": "add",
    "itemid": "0",
    "forward": "http://wuliu.akng.net/admin.php?moduleid=63&wxptinfo=1",
    "post[catid]": "1",
    "post[wxptinfo]": "1",
    "post[wxpt_id]": "14",
    "post[title]": "微信测试1",
    "post[level]": "0",
    "post[style]": "",
    "post[thumb]": "",
    "post[content]": "",
    "post[introduce_length]": "120",
    "post[thumb_no]": "",
    "post[subtitle]": "",
    "post[introduce]": "",
    "post[author]": "",
    "post[copyfrom]": "",
    "post[fromurl]": "",
    "post[tag]": "",
    "post[voteid]": "",
    "post[status]": "3",
    "post[note]": "",
    "post[addtime]": "2021-12-21 15:23:03",
    "post[areaid]": "0",
    "post[hits]": "0",
    "post[fee]": "",
    "post[template]": "",
    "submit": "确 定",
}

WxArticlePayloadKeys = {
    "moduleid": "moduleid",
    "file": "file",
    "action": "action",
    "itemid": "itemid",
    "forward": "forward",
    "catid": "post[catid]",
    "wxptinfo": "post[wxptinfo]",
    "wxpt_id": "post[wxpt_id]",     # catid
    "title": "post[title]",
    "level": "post[level]",
    "style": "post[style]",
    "thumb": "post[thumb]",
    # "linkurl": "post[linkurl]",
    "content": "post[content]",
    "save_remotepic": "post[save_remotepic]",
    # "clear_link": "post[clear_link]",
    "introduce_length": "post[introduce_length]",
    "thumb_no": "post[thumb_no]",
    "subtitle": "post[subtitle]",
    "introduce": "post[introduce]",
    "author": "post[author]",
    "copyfrom": "post[copyfrom]",
    "fromurl": "post[fromurl]",
    "tag": "post[tag]",
    "voteid": "post[voteid]",
    "status": "post[status]",
    "note": "post[note]",
    "addtime": "post[addtime]",
    "areaid": "post[areaid]",
    "hits": "post[hits]",
    "fee": "post[fee]",
    "template": "post[template]",
    "submit": "submit"
}