# -*- coding: utf-8 -*-
# CreateDate : 2021/12/20 21:56
# Author     : 不肖生
# Github     : https://gitee.com
# EditDate   : 
# sourceProj : wuliu_news
# Description:
import json
import os.path

from wxgzh_news.constant import Default_User_Agent

ProjectPath = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


class WxApi:
    def __init__(self, token=None):
        if token is None:
            token = self.load_token()
        self.token = str(token)

        self.cookies = self.load_cookies()

        self.headers = {
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6",
            "referer": f"https://mp.weixin.qq.com/cgi-bin/appmsg?t=media/appmsg_edit_v2&action=edit&isNew=1&type=10&createType=0&token={self.token}&lang=zh_CN",
            "sec-ch-ua-platform": "macOS",
            "sec-fetch-site": "same-origin",
            "user-agent": Default_User_Agent,
            "x-requested-with": "XMLHttpRequest"
        }

    @staticmethod
    def load_token():
        """
        :return:
        """
        with open(os.path.join(ProjectPath, "token.txt"), "r") as fp:
            token = fp.read().strip()
        return token

    def load_cookies(self):
        """
        :return:
        """
        with open(os.path.join(ProjectPath, "cookies.txt"), 'r', encoding="utf-8") as fp:
            cookie_string = fp.read()
        try:
            cookies = json.loads(cookie_string)
        except Exception as e:
            cookies = self.format_cookies(cookie_string)

        # cookies.pop("appmsglist_action_3940004496", "")
        # cookies.pop("ptui_loginuin", "")
        # cookies.pop("mm_lang", "")
        # cookies.pop("noticeLoginFlag", "")
        # cookies.pop("uuid", "")
        # cookies.pop("pgv_pvid", "")
        return cookies

    @staticmethod
    def format_cookies(cookie_string):
        """
        :param cookie_string: rewardsn=; wxtokenkey=777; ua_id=stAi2A2leXjFhF7LAAAAAHkOJtTkjO4Lkv0HuSm1owU=; wxuin=39994182228951; cert=wFcmuu_M75ePUOidElM6NmKSjEAUMuyL; pgv_info=ssid=s1178384930; pgv_pvid=9644830440; ptui_loginuin=1405762469; uin=o1405762469; skey=@McbW4Xtpf; RK=KgaEnuLOMH; ptcz=9af0257414a988f4c644671cdf7e2826c5fb8a47b139a2cdba861a6ce64d036e; master_key=STvGFTDPHSAW/0k8myosCDW7+lZNT153Oupl3dFrgFU=; mm_lang=zh_CN; noticeLoginFlag=1; sig=h01931cd634619b051a92d33f1cfd63fde1a7998788cc716aea06492c078c02e10ebe1e42665978e26e; remember_acct=17600123099%40163.com; uuid=c59380ad94c7fd28cabdd8fa76b6af98; rand_info=CAESIODcPaZ100Wr+TrJlGnyyDAhEYK1rbcjheSI/Sq86kqn; slave_bizuin=3940004496; data_bizuin=3940004496; bizuin=3940004496; data_ticket=RN7JCBPGc840qJ5JvHjsJsEBdqgiOvRgASRxu1MF7LPStYKgBbuL4zhBKI6wqF5e; slave_sid=QlhFbUxYekNjUm5LbFIyTVZYZWFORUtwZUpudzk2aUNhWFBfb1hTbm1Za1ptY29iMDQ3XzhkZTdJZDlDU0hSSV9Tb3lGNnV6TUl0bDRIZzNrNDR1ZEVWQ2hFeDVLdjZkWXh2N3N5emhkaUxiWDBtU2F5SDRNODRJd2dJaGVPRlptenJ6eGY5eUVCQzlWeGRy; slave_user=gh_fcf004a90a8a; xid=921c88372400a2e134a197745ec4662a
        :return:
        """
        cookies = {}
        for c_item in cookie_string.split(";"):
            if not c_item.strip():
                continue
            ckv = c_item.split("=", maxsplit=1)
            cookies[ckv[0].strip()] = ckv[1].strip()
        return cookies


if __name__ == '__main__':
    ...

